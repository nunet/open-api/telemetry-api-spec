# Telemetry API Specification

This repository shows the API Spec for the Telementry API. 

The generated documentation links can be found here:
- Stable (Main Branch) https://openapi.nunet.io/telemetry-api-spec/main/asyncapi/
- Beta (Staging Branch) https://openapi.nunet.io/telemetry-api-spec/staging/asyncapi
- Alpha (Develop Branch) https://openapi.nunet.io/telemetry-api-spec/develop/asyncapi