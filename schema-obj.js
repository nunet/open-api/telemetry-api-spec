const myStructData = {
  DeviceResource: ['peer-id-of-compute-host', 1200, 1500, 17, 23, 234878],

  ServiceResource: ['peer-id-of-compute-host', 'peer-id-of-service-host', 1200, 2000, 1500, 17, 23, 234878],

  NewDeviceOnboarded: {
    resources: ['peer-id-of-compute-host', 1200, 1500, 17, 23, 234878],
  },

  DeviceResourceChange: {
    resources: ['peer-id-of-compute-host', 1200, 1500, 17, 23, 234878],
  },

  DeviceResourceConfig: {
    resources: ['peer-id-of-compute-host', 1200, 1500, 17, 23, 234878],
  },
  
  DeviceStatus: {
    peer_id: 'peer-id-of-compute-host',
    status:  'on-or-off',
    reason: 'particular reason of operation',
    timestamp: 234878,
  },
  
  NewService: {
    service_id: 'test-service-id',
    service_name:  'service_name',
    service_description: 'Description of service',
    timestamp: 234878,
  },
  
  ServiceCall: {
    call_id: 1737162,
    service_id: 'test-service-id',
    resources: ['peer-id-of-compute-host', 'peer-id-of-service-host', 1200, 2000, 1500, 17, 23, 234878],
    status: 'status of service',
    amount_of_ntx: 25,
  },
  
  ServiceStatus: {
    call_id: 1737162,
    peer_id_of_service_host: 'peer-id-of-service-host',
    service_id: 'test-service-id',
    status: 'status of service',
    timestamp: 234878,
  },
  
  ServiceRemove: {
    service_id: 'test-service-id',
    timestamp: 234878,
  },
  
  NtxPayment: {
    call_id: 1737162,
    peer_id_of_service_host: 'peer-id-of-service-host',
    service_id: 'test-service-id',
    amount_of_ntx: 25,
    success_fail_status: 'status of transaction',
    timestamp: 234878,
  },

  HearBeat: {
    peer_id: 'peer-id-of-compute-host',
  },
};

export default myStructData;